import React, {useState} from 'react';
import './home.css'
import { faHeadphones } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import MenuPage from './menu-page';

export default function Home(props){
    const [show,updateShow]=useState(false)
    var count = 0;
    function getmenu(){       
        
        if (count%2===0){  
            updateShow(true);
            }else{
              
                updateShow(false);  
            }
            count+= 1;      
        //alert(count);
          
        
           
    }
   
    
    return(
        <div>
            <MenuPage show={show}/>
        <div className="home">
            <div className="back-page">
            <div className="header">
                <div className="header-content">Mix Legno Group</div>                
                <div className="header-content">
                <div  className="header-button"
                onClick={()=>getmenu()}>
                <div className="header-inner-content"></div> 
                <div className="header-inner-content"></div>
                </div>
                </div>                
            </div>
            <div className="square-one">
                <div className="square-two"> 
                <div className="square-three">
                    <div className="square-three-text-one">Chapter One</div>   
                    <div className="square-three-text-ref">Reflect</div>  
                
                </div>
                </div>
            </div>
            <div className="footer">
                <div className="footer-content-one">Show Chapters</div>
                <div className="footer-content-two">1 / 4</div>
                <div className="footer-content-three">
                <FontAwesomeIcon icon={faHeadphones} />
                </div>
            </div>
            
            </div>
            
        </div>
        </div>
    );
}